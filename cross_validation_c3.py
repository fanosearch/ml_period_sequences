# Script to generate cross-validation maps, that help
# to evaluate the performance of a model by computing
# its score for several values of the parameters.

from functools import reduce
from math import ceil
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import pickle
from scipy.stats import gaussian_kde
import sqlite3 as sql
import sys
from sklearn.ensemble import AdaBoostRegressor
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import MinMaxScaler
from sklearn.svm import SVR


# We need python 3.6 or later because we rely
# on dictionaries preserving the order in which
# the keywords are inserted.
assert sys.version_info >= (3, 6)


# Inference target.
target = 'h2'
# Features to be used for inference. This should be
# a list even if you plan to use a single feature.
features = ['slope', 'offset', 'S1', 'S2']
# Use this dictionary if you need to put some constraints
# on the data used for inference.
constraints = {'fano_index': 1}
# Seed for pseudo-random number generators.
random_state = 0
# Number of points in each direction for the 
# cross-validation grid search
n_grid_search = 15
# Fraction of the dataset to use for training.
# This parameter is not used for the cross-validation, but
# only later when comparing the best model for each class
# of predictors.
training_fraction = 0.9
# Plots the level set that contains (approximately) mass_fraction
# of the total mass.
mass_fraction = 0.9
# Models to compare.
models = {
          'linear':
              Ridge(tol=1.e-6),
          'svm_rbf':
              SVR(kernel='rbf',
                  cache_size=4000),
          'svm_poly':
              SVR(kernel='poly',
                  cache_size=4000),
          'boosting_linear':
              AdaBoostRegressor(base_estimator=LinearRegression(),
                                random_state=random_state)
}
# Cross-validation parameters for each model.
cv_params = {
             'linear':
                 {'alpha': np.logspace(-2, 1, num=n_grid_search)},
             'svm_rbf':
                 {'C': np.logspace(-4, 0, num=n_grid_search),
                  'gamma': np.logspace(-3, 1, num=n_grid_search)},
             'svm_poly':
                 {'C': np.logspace(-4, 0, num=n_grid_search),
                  'gamma': np.logspace(-4, 0, num=n_grid_search)},
             'boosting_linear':
                 {'n_estimators': np.linspace(20, 100, num=n_grid_search, dtype=np.int32),
                  'learning_rate': np.logspace(-3, 0, num=n_grid_search)},
}
# Plot descriptions for visualising the cross-validation results.
cv_title = {
    'linear':
    'Validation accuracy for Ridge regressor',
    'svm_rbf':
    'Validation accuracy for SVR with gaussian kernel',
    'svm_poly':
    'Validation accuracy for SVR with polynomial kernel',
    'boosting_linear':
    'Validation accuracy for AdaBoost-linear regressor',
 }


conn = sql.connect('c3.db')
query = 'SELECT ' + ', '.join(features) \
        + ', ' + ', '.join(constraints.keys()) \
        + f', {target} from periods'
df = pd.read_sql_query(query, conn)


masks = []
for k, v in constraints.items():
    print(f'Constraint: {k} == {v}')
    masks.append(df[k] == v)
if masks:
    mask = reduce(lambda x, y: x & y, masks)
    # replaces the masked values with Nans
    df.where(mask, inplace=True)
# drop nans
df.dropna(inplace=True)

sc = MinMaxScaler()
df[features] = sc.fit_transform(df[features])

# It could also be useful to rescale the target vector for later.
rtarget = f'r{target}'
df[rtarget] = sc.fit_transform(df[[target]])


# Full data for cross-validation
indices = np.arange(df.shape[0], dtype=np.intp)
rng = np.random.default_rng(0)
rng.shuffle(indices)
X = df[features].iloc[indices]
y = df[rtarget].iloc[indices].to_numpy(),
y = y[0]


assert os.path.isdir('./c3'), 'Please create a directory called "c3" in the current path so that we can save the results of the cross-validation run.'
   

### Main loop over the models.
cv_res = {}
cm = plt.get_cmap('tab20c')
colors = [cm(i/20) for i in range(n_grid_search)]
for model_name, model in models.items():
    params = cv_params[model_name]
    clf = GridSearchCV(model, params, n_jobs=-1)
    clf.fit(X, y)
    cv_res[model_name] = clf.cv_results_
    cur_res = cv_res[model_name]
    
    with open(f'c3/cv_res_{target}_{model_name}.pickle', 'wb') as f:
        pickle.dump(cv_res, f)
    
    # Plotting stuff
    fig, ax = plt.subplots()
    Ks = list(params.keys())
    Cs = params[Ks[0]]
    if len(params) == 1:
        mean_test_score = cur_res['mean_test_score']
        std_test_score = cur_res['std_test_score']
        ax.errorbar(np.log10(Cs), mean_test_score, yerr=std_test_score)
        ax.set_xlabel(Ks[0])
        ax.set_ylabel('score')
        ax.set_title(cv_title[model_name])
    else:
        for j, g in enumerate(params[Ks[1]]):
            mask = cur_res[f'param_{Ks[1]}'] == g
            mean_test_score = cur_res['mean_test_score'][mask]
            std_test_score = cur_res['std_test_score'][mask]
        
        
            if Ks[0] == 'C':
                ax.errorbar(
                     np.log10(Cs),
                     mean_test_score,
                     yerr=std_test_score,
                     color=colors[j],
                     label=r'$\log_{10}$' + f'{Ks[1]} = {np.log10(g):.4}')
                ax.set_xlabel(r'$\log_{10}$' + Ks[0])
            else:
                ax.errorbar(
                    Cs,
                    mean_test_score,
                    yerr=std_test_score,
                    color=colors[j],
                    label=f'{Ks[1]} = {np.log10(g):.4}')
                ax.set_xlabel(Ks[0])
            ax.set_ylabel('score')
            ax.set_title(cv_title[model_name])
            ax.legend(loc='best')

    plt.savefig(f'c3/cv_{target}_{model_name}', dpi=400)


### Find the model with the best score.
def montecarlo_approx(pdf, mass_fraction, n_samples=2**16):
    ''' Returns the approximate level set of pdf that contains
    ~ mass_fraction of the total mass, using a very raw
    Montecarlo method.'''
    sample = pdf.resample(size=n_samples, seed=random_state)
    values = pdf(sample)
    values.sort()
    return values[ceil((1 - mass_fraction)*n_samples)]


print(f'Features: {features}')
print(f'Target: {target}')
for model_name, results in cv_res.items():
    i = np.argmin(results['rank_test_score'])
    mean = results['mean_test_score'][i]
    std = results['std_test_score'][i]
    params = results['params'][i]
    print(f'Model: {model_name}')
    print(f'  best test score: {mean:.4} +- {std:.4}.')
    print(f'  with parameters:')
    for par, val in params.items():
        if isinstance(val, np.int32):
            print(f'    {par}: {val}')
        else:
            print(f'    {par}: {val:.4}')

    # Train the best model for each class and
    # plot the predictions against the real values.
    model = models[model_name]
    for par, val in params.items():
        setattr(model, par, val)

    # Generate train and test sets
    N = X.shape[0]
    # Number of rows to use for training.
    NT = ceil(training_fraction*N)
    rng.shuffle(indices)
    X_raw = X.to_numpy()
    X_train = X_raw[indices[:NT], :]
    X_test = X_raw[indices[NT:], :]
    y_train, y_test = y[indices[:NT]], y[indices[NT:]]
    model.fit(X_train, y_train)
    prediction = model.predict(X_test)

    fig, ax = plt.subplots()
    ax.scatter(y_test, prediction, c='k', alpha=0.1)
    # approximate distribution density
    yps = (min(y_test.min(), prediction.min()),
           max(y_test.max(), prediction.max()))
    yr = np.linspace(*yps, 2**8)
    pdf = gaussian_kde(np.vstack([y_test, prediction]))
    # We estimate the integral of the pdf by a very raw
    # Montecarlo method. We only need to find out the (approximate)
    # level-set of the pdf that bounds mass_fraction of the total mass.
    level = montecarlo_approx(pdf, mass_fraction)
    xplot, yplot = np.meshgrid(yr, yr)
    pdf_vals = pdf(np.vstack((xplot.ravel(), yplot.ravel()))).T.reshape(xplot.shape)
    ax.contour(xplot,
               yplot,
               pdf_vals,
               [level])
    # draw straight line
    ax.plot(yr, yr, '--k')
    ax.set_xlabel('test value')
    ax.set_ylabel('prediction')
    ax.set_title(cv_title[model_name] + f' for target: {target}')
    ax.set_aspect('equal', 'box')
    fig.tight_layout()
    plt.savefig(f'c3/prediction_{target}_{model_name}', dpi=400)



print('All done.')

