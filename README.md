[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2
/git/https%3A%2F%2Fbitbucket.org%2Ffanosearch%2Fml_period_sequences%2Fsr
c%2Fmaster/HEAD)

Click on the binder badge at the top to connect to the cloud service 
with the jupyter notebooks.
If you do not see the badge, you might need to remove the 
`static.mybinder.org` domain from your adblocker/browser blacklist...


## Python scripts

# Cross validation and model selection

The usual pipeline to generate and evaluate a ML model consists in:

1. choosing the target variable.
2. Choosing the possible predictors.
3. Choosing a set of statistical models and a sensible range for their 
   parameters (if applicable).
4. Evaluating the performance of each model in a set of representative 
   points for its parameters's range. This is done by cross-validation.
5. Choosing the best-performing model for each class, and compare their 
   relative strengths and weaknesses.

All these steps (except the comparison, which requires some human 
interpretation), are done automatically by the script 
`cross_validation_c3.py`.
Here, the suffix `_c3` suggests that this script was designed and 
tested for the database of period sequences for the canonical 3-topes.

The models explored in this work are:

- **Ridge regression**. [This
  model](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Ridge.html#sklearn.linear_model.Ridge)
  attempts to find the best affine
  predictor $y = X w + b$ by minimising a functional $L$ of the
  unknowns $w$ and $b$:

      $L(w, b) = \|y - (X w + b)\|_2^2 + \alpha(\|w\|_2^2 + \|b\|_2^2$.
  
  The case $\alpha = 0$ recovers the best affine fit in the
  least-squares sense.

- **Support Vector Machines**. Support Vector Machines for
  regression problems are sometimes called [Support Vector
  Regressors or just
  SVRs](https://scikit-learn.org/stable/modules/svm.html#regression).
  The SVR attempts to find a best linear fit after mapping the data
  to a usually higher dimensional space using a function $\phi$.
  Usually instead of $\phi$ the SVM is specified by a kernel
  function $K(x, x') = \phi(x)\cdot\phi(x')$ induced by $\phi$.
  In this work we consider both gaussian kernels:

      $K(x, x') = exp(-\gamma <x - x', x - x'>)$,
  
  and polynomial kernels of degree 3:
  
      $K(x, x') = (\gamma <x, x'> + r)^3$,
  
  here we consider the default value $r = 0$.
  When inspecting the cross-validation results to choose between
  gaussian and polynomial kernel, keep in mind that the gaussian kernel
  has one fewer parameter than the polynomial kernel (not really true
  because we always use $r = 0$, but in principle one could tune the
  model on that additional parameter as well. By Occam's razor, if the
  CV results of the two models are close, I would prefer the model with
  gaussian kernel.
  A very closely related model is the Kernel Ridge Regression, not
  considered here, but [read this for
  more](https://scikit-learn.org/stable/modules/generated/sklearn.kernel_ridge.KernelRidge.html#sklearn.kernel_ridge.KernelRidge).

- **AdaBoost**. The [AdaBoost regressor](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.AdaBoostRegressor.html?highlight=boost#sklearn.ensemble.AdaBoostRegressor)
  is based on the idea of fitting another regressor (a Linear model in
  this case) several times on the same dataset, but giving larger
  weight to the data points where the previous estimator makes the
  larger errors. As a result of the training process, the boosting model
  ends up with several instances of the same model. When the boosting
  model is used to make a prediction, it will evaluate a weighted mean
  of all the base models it has found during the training phase.

Some more things that it could be useful to keep in mind are:

- before training the models, both the features and the target are
  rescaled to the interval [0, 1].
- If you run out of memory, try reducing the optional keyword parameter
  `cache_size` used in the constructors of the `SVR`s.
- If the script is using too many cores, change the parameter `n_jobs`
  in the constructor of `GridSearchCV` to something sensible. The
  default parameter, -1, means to use all available cores.

# Tasks to explore

- Is it better to train a model using the (possibly rescaled) logarithms
  of the periods, or the coefficients of the asymptotic expansion
  $\log c_k + 3/2 \log k = Ak + B + \sum_i e_i/k^i$?
- How does the size of the training set affect the prediction accuracy?
- Can we predict anything at all from the period sequences?


# Example


Prediction of the second component of the Hilbert series (`h2` in the
database) from the parameters $A, B, e_1, e_2$ (called respectively
`slope, offset, S1, S2` in the database).

Target | best score (Ridge) | best score (SVR, gaussian) | best score (SVR, polynomial) | best score (AdaBoost) 
------|------------|-------- ------|------------|--------
h1  | 0.8852 +- 0.007098 | 0.9255 +- 0.0142  | 0.934 +- 0.01185 | 0.9147 +- 0.003907
h2  | 0.8858 +- 0.007219 | 0.9216 +-0.01535   | 0.9275 +- 0.01522 | 0.9137 +- 0.004151
h3  | 0.885  +- 0.007147 | 0.9196 +- 0.007284 | 0.9289 +- 0.01474 | 0.9127 +- 0.004058
h4 | 0.8849 +- 0.00716 | 0.9208 +- 0.007295 | 0.9291 +- 0.01644 | 0.9124 +- 0.004076
degree | 0.8844 +- 0.007153 | 0.9234 +- 0.007224 | 0.9303 +- 0.01862| 0.9118 +- 0.004082
picard_rank | 0.4756 +- 0.01806 | 0.4883 +- 0.02307 | 0.4823 +- 0.02126 | 0.4871 +- 0.01917
symbol_degree | 0.3427 +- 0.02263 | 0.3555 +- 0.02617 | 0.3522 +- 0.02513 | 0.3594 +- 0.02295

We can also try to predict the Picard rank for the period sequences
for a given value of the Gorenstein index. In this way we have fewer
data points, but maybe this is going to be a simpler type of inference.

Gorenstein index | # data points | best score (Ridge) | best score (SVR, gaussian) | best score (SVR, polynomial) | best score (AdaBoost) 
----|------|------------|-------- ------|------------|--------
2  |  738 | 0.06965 +- 0.02078 | 0.08128 +- 0.02546 | 0.06464 +- 0.03206 | 0.06994 +- 0.02027
4  |  784 | 0.1869 +- 0.04445 | 0.2368 +- 0.04194 | 0.1766 +- 0.04294 | 0.1868 +- 0.04313
6  | 2661 | 0.215 +- 0.04178 |  0.2653 +- 0.06379 | 0.213 +- 0.05591 | 0.2102 +- 0.04547
12 | 1079 | 0.243 +- 0.0236 | 0.2664 +- 0.03901 | 0.2382 +- 0.02088 | 0.2535 +- 0.02483


An alternative way to judge the performance of a predictor is to plot
the points (target_i, prediction_i) for all `i`, and look at how
aligned is the resulting distribution (in the plots below, the area
inside the contour holds approximately 90% of the total estimated mass).

![Boosting method for h2](c3/prediction_h2_boosting_linear.png){width=640 height=480}
![Boosting method for the Picard rank](c3/prediction_picard_rank_boosting_linear.png){width=640 height=480}
![Boosting method for the degree of the symbol of the Picard-Fuchs operator](c3/prediction_symbol_degree_boosting_linear.png){width=640 height=480}


## Database tools
If you want to experiment quickly with the periods data without worrying
about dealing with the complexity of mongo, you can fit the asymptotic
expansion for the growth rate of the period sequences using the scripts
in the folder `tools` as follows.

1. Copy the contents of the folder `tools` on a machine where you have
   access to pcas-magma.
2. Create a directory called `output`.
3. Edit the variables `dbname` and `tabname` in the script `long_periods.m`
   as suitable. Figure out how many jobs you need to run to complete the
   task (100 periods for each job), and submit an array job making
   sure to call `pcas-magma -kvdb long_periods.m`.
   This script queries mongo, recovers the Picard-Fuchs operator
   (which must be available from the table you are querying), and
   generates a lot of periods, whose logarithms are then stored in the
   folder `output` in text format.
4. Run `lsq_fit.py`. This scripts reads the periods from the `output`
   folder and estimates the coefficients for the asymptotic expression.
   It also computes the Fano index of the period sequence.
   The results will be available on a SQLite database whose name can
   be specified in the variable `dbname`.
   This script has been tested on ~8000 period sequences,
   and it is fast enough that it could easily run on a single core.
   Your might consider distributing this computation among many cores
   if you need to process a lot of period sequences.
