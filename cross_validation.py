# Script to generate cross-validation maps, that help
# to evaluate the performance of a model by computing
# its score for several values of the parameters.

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
import sqlite3 as sql
from sklearn.decomposition import PCA
from sklearn.ensemble import AdaBoostRegressor
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import MinMaxScaler
from sklearn.svm import SVR

conn = sql.connect('deltas.db')
query = 'SELECT delta1, delta2, period from terminals'
df = pd.read_sql_query(query, conn)


random_state = 0
# Keep n_components of the PCA
n_components = 3
# Number of points for the cross-validation grid search
n_grid_search = 12
# Models to compare.
models = {
          'svm_rbf':
              SVR(kernel='rbf',
                  cache_size=2_000),
          'svm_poly':
              SVR(kernel='poly',
                  cache_size=2_000),
          'boosting_linear':
              AdaBoostRegressor(base_estimator=LinearRegression(),
                                random_state=random_state),
          'boosting_svm':
              AdaBoostRegressor(base_estimator=SVR(kernel='rbf',
                                                   C=1.,
                                                   gamma=3.6,
                                                   cache_size=2_000),
                                random_state=random_state)
}
# Cross-validation parameters for each model.
cv_params = {'svm_rbf':
                 {'C': np.logspace(-4, 0, num=n_grid_search),
                  'gamma': np.logspace(-1.5, 2, num=n_grid_search)},
             'svm_poly':
                 {'C': np.logspace(-4, 0, num=n_grid_search),
                  'gamma': np.logspace(-1.5, 1, num=n_grid_search)},
             'boosting_linear':
                 {
                  'learning_rate': np.logspace(-3, 0, num=n_grid_search),
                  'n_estimators': np.linspace(50, 500, num=n_grid_search, dtype=np.int32)
                 },
             'boosting_svm':
                 {
                  'learning_rate': np.logspace(-3, 0, num=n_grid_search),
                  'n_estimators': np.linspace(50, 500, num=n_grid_search, dtype=np.int32)
                 }
}
# Plot descriptions for visualising the cross-validation results.
cv_title = {
    'svm_rbf':
    'Validation accuracy for SVR with gaussian kernel',
    'svm_poly':
    'Validation accuracy for SVR with polynomial kernel',
    'boosting_linear':
    'Validation accuracy for AdaBoost-linear regressor',
    'boosting_svm':
    'Validation accuracy for AdaBoost-SVR with gaussian kernel'
}

# Choose a starting index for the period sequences
# (we might have to throw away a few terms to get rid
# of the occasional zero period that would be mapped to
# infinity under the log map.)
n_start = 16


def split_cols(x, n_start=0):
    ''' Transforms period sequences from text to tables.
    It neglects the first n_start terms of the period
    sequence. '''
    terms = x.rstrip(']').split(',')
    # if there are more than one zeros in the period sequence,
    # we are going to remove it from the database, so that we
    # can later have a cleaner Principal Components Decomposition.
    if sum([t == '0' for t in terms]) > 1:
        return np.array([np.nan for _ in terms[n_start:]])
    return np.array([np.log(float(t)) for t in terms[n_start:]])


# Count the number of terms of the period sequence
n_terms = len(df['period'][0].split(','))
ps_cols = [f'ps{i}' for i in range(n_start, n_terms)]


split_start = lambda x: split_cols(x, n_start=n_start)
vec_split_cols = np.vectorize(split_start)
data = np.array([vec_split_cols(x) for x in df['period'].values])

df_comp = pd.DataFrame(data,
                       columns=ps_cols,
                       index=df.index)

# drop NaNs or Infs that could have appeared for the Fano indexes > 1.
mask = np.any(np.isnan(df_comp) | np.isinf(df_comp), axis=1)
print(mask.shape)
# replace with nan values where ~mask is False
df_comp.where(~mask, inplace=True)
df_comp.dropna(inplace=True)

sc = MinMaxScaler()
df_comp[ps_cols] = sc.fit_transform(df_comp[ps_cols])

# It could also be useful to rescale the delta vector for later.
col_deltas = ['rdelta1', 'rdelta2']  # rescaled deltas, components 1 and 2.
df_deltas = pd.DataFrame(index=df.index)
df_deltas[col_deltas] = sc.fit_transform(df[['delta1', 'delta2']])

# inner join of the two dataframes: only keep the common rows.
df = df.join(df_comp, how='inner')
df = df.join(df_deltas, how='inner')
# we don't need df_comp and df_deltas anymore
del df_comp
del df_deltas


## PCA on the period sequences
pca = PCA(random_state=random_state)
pca.fit(df[ps_cols])

ps_mapped = pca.transform(df[ps_cols])
# rescale after mapping to the principal component space
ps_mapped = sc.fit_transform(ps_mapped[:, :n_components])

pca_cols = [f'X{i}' for i in range(1, n_components + 1)]
df_mapped = pd.DataFrame(ps_mapped,
                         columns=pca_cols,
                         index=df.index)

df = df.join(df_mapped, how='inner')


# Full data for cross-validation
indices = np.arange(df.shape[0])
rng = np.random.default_rng(0)
rng.shuffle(indices)
X = df[pca_cols].iloc[indices]
y = [df['rdelta1'].iloc[indices],
     df['rdelta2'].iloc[indices]]



## Main loop over the models.
for model_name, model in models.items():
    params = cv_params[model_name]
    clf = GridSearchCV(model, params, n_jobs=-1)
    clf.fit(X, y[0])
    cv_res = clf.cv_results_
    
    with open(f'cv_res_{model_name}.pickle', 'wb') as f:
        pickle.dump(cv_res, f)
    
    fig, ax = plt.subplots()
    Ks = sorted(list(params.keys()))
    Cs = params[Ks[0]]
    for j, g in enumerate(params[Ks[1]]):
        mask = cv_res[f'param_{Ks[1]}'] == g
        mean_test_score = cv_res['mean_test_score'][mask]
        std_test_score = cv_res['std_test_score'][mask]
    
    
        cm = plt.get_cmap('tab20c')
        colors = [cm(i/20) for i in range(n_grid_search)]
        if Ks[0] == 'C':
            ax.errorbar(np.log10(Cs),
                        mean_test_score,
                        yerr=std_test_score,
                        color=colors[j],
                        label=r'$\log_{10}\gamma$' + f' = {np.log10(g):.4}')
        else:
            ax.errorbar(np.log10(Cs),
                        mean_test_score,
                        yerr=std_test_score,
                        color=colors[j],
                        label=f'n_estimators = {g}')
        ax.set_xlabel(r'$\log_{10}$' + Ks[0])
        ax.set_ylabel('score')
        ax.set_title(cv_title[model_name])
        ax.legend(loc='best')
        plt.savefig(f'cv_{model_name}', dpi=400)

print('All done.')

