// Gets all the period sequences with id from a to b from the
// key-value database, reconstructs the period sequence,

dbname := "canonicals3";
table := "periods";
features := {"id", "period", "picard_fuchs_coefficients", "picard_fuchs_exponents"};

job_id := StringToInteger(GetEnv("PBS_ARRAY_INDEX"));
a := (job_id - 1)*100 + 1;
b := a + 99;
W<D,t>:=WeylAlgebra();

function file_exists(id)
    outfile := Sprintf("output/%o.txt", id);
    ok, _ := OpenTest(outfile, "r");
    return ok; 
end function;


db := KeyValueDbConnect(dbname : backend:="kvdb");

for id in [a..b] do
    // Check if we already know this picard fuchs operator.
    if file_exists(id) then continue; end if; 
    selector_pf := Sprintf("WHERE id = %o and picard_fuchs_coefficients != \"\"", id); 
    
    for item in KeyValueDbProcess(db, table, features, selector_pf) do
        if not IsDefined(item, "picard_fuchs_coefficients") then
            error "The period sequence you are trying to access does not have information about the Picard-Fuchs operator.";
        end if; 
        ps_id := eval(item["id"]);
        assert ps_id eq id; 
        ps_short := eval(item["period"]);
        cs := eval(item["picard_fuchs_coefficients"]);
        ex := eval(item["picard_fuchs_exponents"]);
        PF := Zero(W);
        for i in [1..#ex] do
            PF +:= cs[i]*t^ex[i][1]*D^ex[i][2];
        end for;
        ps := PeriodSequenceForPicardFuchsOperator(PF, 2000, ps_short);
    
        // indices (will be used for least squares fitting)
        ks := [k : k in [1500..2000] | ps[k] gt 0]; 
        // logs of the periods for the indices.
        ck := [RealField() | Log(ps[k]) : k in ks];
    end for;
    
    F := Open(Sprintf("output/%o.txt", id), "w");
    fprintf F, "%o: %o %o\n", id, ToString(ks), ToString(ck);
end for;

print "All done.";

quit;
