# Fills a SQLite database with sloes and offsets of a linear fit
# of the logs of the period sequences.
import numpy as np
import os
from scipy.linalg import lstsq, LinAlgError
import sqlite3 as sql 

# We fit the coefficients of the negative powers of k up
# to and including the term of order -k_max.
k_max = 4 
dbname = 'canonicals3.db'

assert not os.path.isfile(dbname), f'Please remove the existing "{dbname}" file before proceeding.'

conn = sql.connect(dbname)
c = conn.cursor()
query = 'CREATE TABLE periods (id integer, slope real, offset real, fano_index integer, '
query += ','.join(f'S{i} real' for i in range(1, k_max + 1)) 
query += ')' 
c.execute(query)

files = os.listdir('output')

err = 0.
for fname in files:
    # Remove the '.txt' suffix.
    n = int(fname[:-4])
    with open(f'output/{fname}', 'r') as f:
        for l in f:
            s = l.split(' ')
            ks = np.array(eval(s[1]), dtype=np.float64)
            ck = np.array(eval(s[2])) + 3/2*np.log(ks)
            # compute the Fano index
            diff_ks = np.roll(ks, 1) - ks
            diff_set = set(diff_ks[1:])
            assert len(diff_set) == 1
            f = abs(diff_set.pop())  # This is the Fano index.
            # Build the mass matrix for the least-squares fit.
            M = ks[:, np.newaxis]**([1., 0.] + [0. for _ in range(k_max)])
            for k in range(k_max):
                M[:, k + 2] = ks**(-k - 1)

            try:
                p, res, rank, s = lstsq(M, ck)
                err = max(err, max(res))
            except LinAlgError as e:
                print('not converging:', e)
            except ValueError as e:
                print('wrong parameters:', e)
            #print(f'residual: {res}')
            #print(f'condition number: {abs(s[0]/s[-1])}')
            m, q = p[:2]
            query = f'INSERT INTO periods VALUES ({n}, {m}, {q}, {f},'
            query += ','.join(f'{c}' for c in p[2:])
            query += ')' 
            c.execute(query)

conn.commit()
conn.close()

print('maximal residual: ', err)

